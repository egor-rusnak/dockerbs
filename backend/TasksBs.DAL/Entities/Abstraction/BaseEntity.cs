﻿using System;

namespace TasksBs.DAL.Entities.Abstraction
{
    public abstract class BaseEntity : IEquatable<BaseEntity>
    {
        public int Id { get; set; }

        public BaseEntity(int id)
        {
            Id = id;
        }

        public bool Equals(BaseEntity other)
        {
            return this.Id == other.Id;
        }
    }
}
