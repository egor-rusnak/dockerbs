﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TasksBs.DAL.Entities.Abstraction;

namespace TasksBs.DAL.Entities
{
    public class Team : BaseEntity
    {
        [StringLength(100)]
        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public IEnumerable<User> Members { get; set; }

        public Team(int id, string name, DateTime createdAt) : base(id)
        {
            Name = name;
            CreatedAt = createdAt;
        }
    }
}
