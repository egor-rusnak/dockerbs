﻿using System;
using System.ComponentModel.DataAnnotations;
using TasksBs.DAL.Entities.Abstraction;

namespace TasksBs.DAL.Entities
{
    public class User : BaseEntity
    {
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime RegisteredAt { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [Required]
        public DateTime BirthDay { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime birthDay, int? teamId) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            BirthDay = birthDay;
            TeamId = teamId;
        }

        public override string ToString()
        {
            return Id + "-" + FirstName + " " + LastName;
        }
    }
}
