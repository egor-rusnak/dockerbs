﻿using System;
using System.ComponentModel.DataAnnotations;
using TasksBs.DAL.Entities.Abstraction;

namespace TasksBs.DAL.Entities
{
    public class Task : BaseEntity
    {
        [StringLength(200)]
        public string Name { get; set; }
        [Required]
        public int PerformerId { get; set; }
        public User Performer { get; set; }

        [Required]
        public int ProjectId { get; set; }
        public Project Project { get; set; }


        [StringLength(500)]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime? FinishedAt { get; set; }

        public Task(int id, string name, int performerId, int projectId, string description, DateTime createdAt, DateTime? finishedAt) : base(id)
        {
            PerformerId = performerId;
            ProjectId = projectId;
            Description = description;
            CreatedAt = createdAt;
            FinishedAt = finishedAt;
            Name = name;
        }
    }
}
