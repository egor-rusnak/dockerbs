﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using TasksBs.DAL.Entities;

namespace TasksBs.DAL.Context
{
    public class ProjectDbContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Project> Projects { get; set; }

        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany<Task>()
                .WithOne(t => t.Performer)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<User>()
                .HasMany<Project>()
                .WithOne(p => p.Author)
                .OnDelete(DeleteBehavior.NoAction);

            SeedData(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            var teams = new List<Team>()
            {
                new Team(1, "Bananas", GetRandomDate()),
                new Team(2, "Changes", GetRandomDate()),
                new Team(3, "Packages", GetRandomDate()),
                new Team(4, "asdas", GetRandomDate())
            };

            var users = new List<User>()
            {
                new User(1, "John", "Jonson", "", GetRandomBirthDayDate() , 1) { RegisteredAt =  GetRandomRegisterDate()},
                new User(2, "Marchel", "Davidson", "", GetRandomBirthDayDate(), 3) { RegisteredAt = GetRandomRegisterDate()},
                new User(3, "Anchey", "Brownly", "",  GetRandomBirthDayDate(), 3) { RegisteredAt = GetRandomRegisterDate()},
                new User(4, "Boris", "Apostolov", "", GetRandomBirthDayDate(), 2) { RegisteredAt = GetRandomRegisterDate()},
                new User(5, "Grace", "Donalds", "", GetRandomBirthDayDate(), 2) { RegisteredAt = GetRandomRegisterDate()},
                new User(6, "George", "Jakson", "", GetRandomBirthDayDate(), null) {RegisteredAt = GetRandomRegisterDate()},
                new User(7, "Annet", "Wonderson", "", GetRandomBirthDayDate(), null) {RegisteredAt = GetRandomRegisterDate()},
                new User(8, "Marcus", "Jonson", "", GetRandomBirthDayDate(), 1) {RegisteredAt = GetRandomRegisterDate()},
                new User(9, "Marcus", "Jonson", "", GetRandomBirthDayDate(), 4) {RegisteredAt = GetRandomRegisterDate()}
            };
            var projects = new List<Project>
            {
                new Project(1, "ProjName1", "Des1", 1, 1, GetRandomDate(), GetRandomDate()),
                new Project(2, "ProjName2", "Des2", 5, 1, GetRandomDate(), GetRandomDate()),
                new Project(3, "ProjName3", "Des3", 8, 2, GetRandomDate(), GetRandomDate()),
                new Project(4, "ProjName4", "Des4", 7, 3, GetRandomDate(), GetRandomDate())
            };

            var tasks = new List<Task>
            {
                new Task(1, "task1", 1, 1, "no", GetRandomDate(), null),
                new Task(2, "task2", 1, 1, "no", GetRandomDate(), GetRandomDate()),
                new Task(3, "task3", 1, 1, "no", GetRandomDate(), null)
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        private DateTime GetRandomRegisterDate()
        {
            return DateTime.Now.AddDays(new Random().Next() % 3000 - 3500);
        }
        private DateTime GetRandomBirthDayDate()
        {
            return DateTime.Now.AddDays(new Random().Next() % 6000 - 6500);
        }
        private DateTime GetRandomDate()
        {
            return DateTime.Now.AddDays(new Random().Next() % 100 - 50).AddMonths(new Random().Next() % 100 - 50).AddYears(new Random().Next() % 10 - 5);
        }

    }
}
