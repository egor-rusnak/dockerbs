﻿using AutoMapper;
using TasksBs.DAL.Context;

namespace TasksBs.BLL.Services.Abstraction
{
    public abstract class BaseSerivce
    {
        protected readonly ProjectDbContext _context;
        protected readonly IMapper _mapper;

        public BaseSerivce(ProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
