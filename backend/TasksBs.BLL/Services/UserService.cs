﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services.Abstraction;
using TasksBs.Common.DTOs.User;
using TasksBs.DAL.Context;
using TasksBs.DAL.Entities;

namespace TasksBs.BLL.Services
{
    public class UserService : BaseSerivce, IUserService
    {
        public UserService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public async Task<UserDto> AddUser(UserDto user)
        {
            var userEntity = _mapper.Map<User>(user);

            await _context.Users.AddAsync(userEntity);

            await _context.SaveChangesAsync();
            return _mapper.Map<UserDto>(await GetUser(userEntity.Id));
        }

        public async Task<UserDto> UpdateUser(UserDto user)
        {
            var userEntity = _mapper.Map<User>(user);

            _context.Users.Update(userEntity);

            await _context.SaveChangesAsync();
            return _mapper.Map<UserDto>(await GetUser(userEntity.Id));
        }

        public async System.Threading.Tasks.Task RemoveUser(int id)
        {
            var userEntity = await GetUser(id);

            if (userEntity == null)
                throw new ArgumentException("No entity with this id!");

            _context.Users.Remove(userEntity);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserDto>> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await GetUsers().ToListAsync());
        }

        public async Task<UserDto> GetById(int id)
        {
            var teamEntity = await GetUser(id);

            if (teamEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<UserDto>(teamEntity);
        }

        private IQueryable<User> GetUsers()
        {
            return _context.Users.Include(u => u.Team);
        }

        private async Task<User> GetUser(int id)
        {
            return await GetUsers().FirstOrDefaultAsync(t => t.Id == id);
        }
    }
}
