﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services.Abstraction;
using TasksBs.Common.DTOs.Project;
using TasksBs.DAL.Context;
using TasksBs.DAL.Entities;

namespace TasksBs.BLL.Services
{
    public class ProjectService : BaseSerivce, IProjectService
    {
        public ProjectService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }


        public async Task<ProjectDto> AddProject(ProjectDto newProject)
        {
            var projectEntity = _mapper.Map<Project>(newProject);

            await _context.Projects.AddAsync(projectEntity);
            await _context.SaveChangesAsync();

            var result = await GetProject(projectEntity.Id);
            return _mapper.Map<ProjectDto>(result);
        }

        public async System.Threading.Tasks.Task RemoveProject(int id)
        {
            var projectEntity = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id); //without includes

            if (projectEntity == null)
                throw new ArgumentException("No entity with this id");

            _context.Remove(projectEntity);
            await _context.SaveChangesAsync();
        }

        public async Task<ProjectDto> UpdateProject(ProjectDto project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            _context.Projects.Update(projectEntity);
            await _context.SaveChangesAsync();

            return _mapper.Map<ProjectDto>(await GetProject(projectEntity.Id));
        }

        public async Task<IEnumerable<ProjectDto>> GetAllProjects()
        {
            var elems = await GetProjects().ToListAsync();
            return _mapper.Map<IEnumerable<ProjectDto>>(elems);
        }

        public async Task<ProjectDto> GetById(int id)
        {
            var projectEntity = await GetProject(id);

            if (projectEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<ProjectDto>(projectEntity);
        }

        private async Task<Project> GetProject(int id)
        {
            var elems = GetProjects();
            return await elems.FirstOrDefaultAsync(p => p.Id == id);
        }

        private IQueryable<Project> GetProjects()
        {
            var projects = _context.Projects.Include(t => t.Team).Include(p => p.Author).Include(p => p.Tasks).ThenInclude(t => t.Performer);
            return projects;
        }
    }
}
