﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;

namespace TasksBs.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<TeamDto> AddTeam(TeamDto team);
        Task<IEnumerable<TeamDto>> GetAllTeams();
        Task<TeamDto> GetById(int id);
        Task RemoveTeam(int id);
        Task<TeamDto> UpdateTeam(TeamDto team);
        Task AddUserToTeam(int teamId, int userId);
    }
}