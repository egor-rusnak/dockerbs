﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;

namespace TasksBs.BLL.Interfaces
{
    public interface ITaskService
    {
        Task<TaskDto> AddTask(TaskDto task);
        Task<IEnumerable<TaskDto>> GetAllTasks();
        Task<TaskDto> GetById(int id);
        Task RemoveTask(int id);
        Task<TaskDto> UpdateTask(TaskDto task);
        Task SetTaskAsFinished(int taskId);
        Task<IEnumerable<TaskDto>> GetUnfinishedTasksByUser(int userId);
    }
}