﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.Common.DTOs.Project;

namespace TasksBs.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<ProjectDto> AddProject(ProjectDto newProject);
        Task<IEnumerable<ProjectDto>> GetAllProjects();
        Task<ProjectDto> GetById(int id);
        Task RemoveProject(int id);
        Task<ProjectDto> UpdateProject(ProjectDto project);
    }
}