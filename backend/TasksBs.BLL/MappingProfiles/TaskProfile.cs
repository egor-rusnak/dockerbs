﻿using AutoMapper;
using TasksBs.Common.DTOs;
using TasksBs.DAL.Entities;


namespace TasksBs.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>()
                .ForMember(m => m.Project, opt => opt.Ignore())
                .ForMember(m => m.Performer, opt => opt.Ignore());

        }
    }
}
