﻿using AutoMapper;
using TasksBs.Common.DTOs.User;
using TasksBs.DAL.Entities;

namespace TasksBs.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>()
                .ForMember(m => m.Team, opt => opt.Ignore());
        }
    }
}
