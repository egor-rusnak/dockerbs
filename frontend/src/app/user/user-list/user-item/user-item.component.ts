import { User } from "./../../../models/user";
import { ObservableUnsubscribe } from "./../../../shared/observable-unsubscribe";
import { UserService } from "./../../../services/user.service";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-user-item",
  templateUrl: "./user-item.component.html",
  styleUrls: ["./user-item.component.scss"]
})
export class UserItemComponent extends ObservableUnsubscribe implements OnInit {

  @Input("item") user: User;

  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();

  constructor(private userService: UserService) { super(); }

  ngOnInit() { }

  remove() {
    if (confirm("Are you sure to delete user?")) {
      this.userService.deleteUser(this.user.id)
        .pipe(takeUntil(this.$unsubscribe))
        .subscribe(() => {
          this.onRemove.emit(this.user.id);
        }, error =>  alert(error.message));
    }
  }

  ngOnDestroy() {
    this.completeSubscription();
  }
}
