import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { takeUntil } from "rxjs/operators";
import { Team } from "src/app/models/team";
import { User } from "src/app/models/user";
import { TeamService } from "src/app/services/team.service";
import { UserService } from "src/app/services/user.service";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";

@Component({
  selector: "app-user-create",
  templateUrl: "./user-create.component.html",
  styleUrls: ["./user-create.component.scss"],
})
export class UserCreateComponent
  extends ObservableUnsubscribe
  implements OnInit {
  $loading: boolean = true;

  newUser: User = {} as User;
  userForm: FormGroup;

  teams: Team[];
  @Output() created = new EventEmitter<User>();
  @Output() canceled = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private teamService: TeamService,
    private userService: UserService
  ) {
    super();
  }

  ngOnInit() {
    this.teamService.getTeams()
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((teams) => {
        this.teams = teams;
        this.$loading = false;
      });

    this.createForm();
  }

  createForm() {
    this.userForm = this.fb.group({
      firstName: ["", [Validators.required, Validators.minLength(2)]],
      lastName: ["", [Validators.required, Validators.minLength(2)]],
      birthDay: ["", Validators.required],
      email: ["", Validators.email],
      team: [],
    });
  }

  onSubmit() {
    this.$loading = true;
    this.newUser = this.userForm.value as User;
    this.newUser.registeredAt = new Date();
    this.userService
      .createUser(this.newUser)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(
        (p) => {
          this.created.emit(p.body);
          this.$loading = false;
          this.userForm.reset();
        },
        (er) => {
          console.log(er);
          this.$loading = false;
        }
      );
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  cancelCreate() {
    this.canceled.emit();
  }
}
