import { UserRoutingModule } from "./user-routing.module";
import { AppPipesModule } from "./../shared/app-pipes.module";
import { MaterialModule } from "./../shared/material.module";
import { SharedModule } from "./../shared/shared.module";
import { UserCreateComponent } from "./user-create/user-create.component";
import { UserItemComponent } from "./user-list/user-item/user-item.component";
import { UserDetailsComponent } from "./user-details/user-details.component";
import { UserListComponent } from "./user-list/user-list.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserComponent } from "./user.component";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    AppPipesModule,
    UserRoutingModule
  ],
  declarations: [UserComponent, UserListComponent, UserDetailsComponent, UserItemComponent, UserCreateComponent]
})
export class UserModule { }
