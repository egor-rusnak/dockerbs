import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { forkJoin } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ComponentCanDeactivate } from "src/app/guards/exit-unsaved.guard";
import { Team } from "src/app/models/team";
import { User } from "src/app/models/user";
import { TeamService } from "src/app/services/team.service";
import { UserService } from "src/app/services/user.service";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"]
})
export class UserDetailsComponent extends ObservableUnsubscribe implements OnInit, ComponentCanDeactivate {

  teams: Team[];

  $loading = true;

  user: User;
  userForm: FormGroup;

  isEdit: boolean = false;
  constructor(
    private userService: UserService,
    private teamService: TeamService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { super(); }

  ngOnInit() {
    const userId = Number.parseInt(this.route.snapshot.paramMap.get("id"));
    forkJoin([this.teamService.getTeams(), this.userService.getUser(userId)])
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(response => {
        this.teams = response[0];
        this.user = response[1];
        this.$loading = false;
      });
  }

  ngOnDesctroy() {
    this.completeSubscription();
  }

  canDeactivate() {
    if (this.isEdit) {
      return confirm("You are editing a user, are you sure, you want to leave not saving him?");
    } else {
      return true;
    }
  }

  editUser() {
    this.createForm();
    this.isEdit = true;
  }

  cancelEdit() {
    this.isEdit = false;
  }

  createForm() {
    this.userForm = this.fb.group({
      id: [],
      firstName: ["", [Validators.required, Validators.minLength(2)]],
      lastName: ["", [Validators.required, Validators.minLength(2)]],
      birthDay: ["", Validators.required],
      email: ["", Validators.email],
      registeredAt: [],
      team: [],
    });
    this.userForm.setValue(this.user);
  }

  onSubmit() {
    this.$loading = true;
    let user = this.userForm.value as User;
    this.userService.updateUser(user)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(() => {
        this.user = user;
        this.isEdit = false;
        this.$loading = false;
      }, error => console.log(error));
  }

  compareTeams(t1: Team, t2: Team) {
    return t1.id === t2.id;
  }
}
