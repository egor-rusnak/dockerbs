import { Team } from "./team";
import { User } from "./user";

export interface Project {
  id: number;
  name: string;
  author: User;
  description: string;
  deadline: Date;
  createdAt: Date;
  team: Team;
}
