import { Project } from "./project";
import { User } from "./user";

export interface Task {
  id: number;
  name: string;
  createdAt: Date;
  finishedAt?: Date;
  project: Project;
  description: string;
  performer: User;
}
