import { Pipe, PipeTransform } from "@angular/core";
import { User } from "../models/user";

@Pipe({
  name: "UserInfo",
})
export class UserInfoPipe implements PipeTransform {
  transform(value: User, args?: any): any {
    return value.id + "-" + value.firstName + " " + value.lastName;
  }
}
