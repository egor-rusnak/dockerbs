import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "UaDate",
})
export class UaDatePipe implements PipeTransform {
  transform(value: Date): string {
    if (value) {
      let options: Intl.DateTimeFormatOptions = {
        day: "numeric",
        month: "long",
      };
      let date = new Date(value);
      return `${date.toLocaleDateString(
        "uk-UA",
        options
      )} ${date.getFullYear()}`;
    }
    return "No date";
  }
}
