import { Subject } from "rxjs";
export class ObservableUnsubscribe {
  protected $unsubscribe: Subject<void> = new Subject<void>();

  protected completeSubscription() {
    this.$unsubscribe?.next();
    this.$unsubscribe?.complete();
  }
}
