import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./shared/material.module";
import { TaskModule } from "./task/task.module";
import { UserModule } from "./user/user.module";
import { TeamModule } from "./team/team.module";
import { ProjectModule } from "./project/project.module";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavigationComponent } from "./navigation/navigation.component";


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    ProjectModule,
    TeamModule,
    UserModule,
    TaskModule,
    MaterialModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
