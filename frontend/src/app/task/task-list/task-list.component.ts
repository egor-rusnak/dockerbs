import { ComponentCanDeactivate } from "./../../guards/exit-unsaved.guard";
import { takeUntil } from "rxjs/operators";
import { ObservableUnsubscribe } from "./../../shared/observable-unsubscribe";
import { TaskService } from "./../../services/task.service";
import { Component, OnInit } from "@angular/core";
import { Project } from "src/app/models/project";
import { Task } from "src/app/models/task";
import { ProjectService } from "src/app/services/project.service";

@Component({
  selector: "app-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.scss"],
})
export class TaskListComponent extends ObservableUnsubscribe implements OnInit, ComponentCanDeactivate {
  cachedTasks: Task[] = [];
  tasks: Task[];

  projects: Project[] = [];

  isCreate = false;

  private _current: Project;
  get current() {
    return this._current;
  }
  set current(project: Project) {
    this._current = project;
    this.filterByProject();
  }

  constructor(
    private projectService: ProjectService,
    private taskService: TaskService
  ) { super(); }

  canDeactivate() {
    if (this.isCreate) {
      return confirm("You are creating task, are you sure, you want to leave? The new task data will be lost.");
    }
    else {
      return true;
    }
  }

  ngOnInit() {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((projects) => {
        this.projects = projects;
        this.taskService
          .getTasks()
          .pipe(takeUntil(this.$unsubscribe))
          .subscribe((tasks) => {
            this.cachedTasks = tasks;
          });
      });
  }

  filterByProject() {
    this.tasks = this.cachedTasks.filter(
      (t) => t.project.id === this.current.id
    );
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  cancelCreate() {
    this.isCreate = false;
  }

  removeTask(id: number) {
    this.cachedTasks = this.cachedTasks.filter(t => t.id != id);
    this.filterByProject();
  }

  createTask(task: Task) {
    this.cachedTasks = this.cachedTasks.concat(task);
    this.tasks = this.tasks.concat(task);
    this.isCreate = false;
  }
}
