import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { ComponentCanDeactivate } from "src/app/guards/exit-unsaved.guard";
import { Team } from "src/app/models/team";
import { TeamService } from "src/app/services/team.service";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";

@Component({
  selector: "app-team-details",
  templateUrl: "./team-details.component.html",
  styleUrls: ["./team-details.component.scss"]
})
export class TeamDetailsComponent extends ObservableUnsubscribe implements OnInit, ComponentCanDeactivate {

  $loading = true;

  team: Team;
  teamForm: FormGroup;

  isEdit: boolean = false;
  constructor(
    private teamService: TeamService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { super(); }

  ngOnInit() {
    const teamId = Number.parseInt(this.route.snapshot.paramMap.get("id"));
    this.teamService.getTeam(teamId)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(team => {
        this.team = team;
        this.$loading = false;
      });
  }

  ngOnDesctroy() {
    this.completeSubscription();
  }

  canDeactivate() {
    if (this.isEdit) {
      return confirm("You are editing a team, are you sure, you want to leave not saving it?");
    } else {
      return true;
    }
  }

  editTeam() {
    this.createForm();
    this.isEdit = true;
  }

  cancelEdit() {
    this.isEdit = false;
  }

  createForm() {
    this.teamForm = this.fb.group({
      id: [],
      name: ["", [Validators.required, Validators.minLength(5)]],
      createdAt: []
    });
    this.teamForm.setValue(this.team);
  }

  onSubmit() {
    this.$loading = true;
    let team = this.teamForm.value as Team;
    this.teamService.updateTeam(team)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(() => {
        this.team = team;
        this.isEdit = false;
        this.$loading = false;
      }, error => console.log(error));
  }
}
