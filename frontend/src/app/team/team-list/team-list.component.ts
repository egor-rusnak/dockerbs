import { TeamService } from "src/app/services/team.service";
import { Component, OnInit } from "@angular/core";
import { ComponentCanDeactivate } from "src/app/guards/exit-unsaved.guard";
import { Team } from "src/app/models/team";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-team-list",
  templateUrl: "./team-list.component.html",
  styleUrls: ["./team-list.component.scss"]
})
export class TeamListComponent extends ObservableUnsubscribe implements OnInit, ComponentCanDeactivate {

  isCreate = false;

  $loading = true;
  teams: Team[];

  constructor(private teamService: TeamService) { super(); }
  canDeactivate() {
    if (this.isCreate) {
      return confirm("You are creating a team, are you shure, you want to leave? The data of new team will be lost.")
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.teamService.getTeams()
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(teams => { this.teams = teams; this.$loading = false; });
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  onRemoveItem(id: number) {
    this.teams = this.teams.filter(p => p.id != id);
  }

  onCreated(team: Team) {
    this.teams = this.teams.concat(team);
    this.isCreate = false;
  }
  onCancel() {
    this.isCreate = false;
  }
}
