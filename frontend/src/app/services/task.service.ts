import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { Task } from "../models/task";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root",
})
export class TaskService {
  apiPrefix: string = "/api/tasks";

  constructor(private httpService: HttpInternalService) { }

  getTasks() {
    return this.httpService
      .getFullRequest<Task[]>(this.apiPrefix)
      .pipe(map((response) => response.body));
  }
  getTask(id: number) {
    return this.httpService
      .getFullRequest<Task>(this.apiPrefix + "/" + id)
      .pipe(map(resp => resp.body));
  }

  createTask(task: Task) {
    return this.httpService.postFullRequest<Task>(this.apiPrefix, task);
  }

  updateTask(task: Task) {
    return this.httpService.putFullRequest(this.apiPrefix, task);
  }

  deleteTask(id: number) {
    return this.httpService.deleteFullRequest(this.apiPrefix + "/" + id);
  }

  finishTask(task: Task) {
    return this.httpService.patchFullRequest(this.apiPrefix + "/finished/" + task.id, task);
  }
}
