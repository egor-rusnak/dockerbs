import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Project } from "../models/project";
import { HttpInternalService } from "./http-internal.service";

@Injectable({
  providedIn: "root",
})
export class ProjectService {
  apiPrefix: string = "/api/projects";

  constructor(private httpService: HttpInternalService) { }

  getProjects() {
    return this.httpService
      .getFullRequest<Project[]>(this.apiPrefix)
      .pipe(map((response) => response.body));
  }

  getProject(id: number) {
    return this.httpService
      .getFullRequest<Project>(this.apiPrefix + "/" + id)
      .pipe(map(resp => resp.body));
  }

  createProject(project: Project) {
    return this.httpService.postFullRequest<Project>(this.apiPrefix, project);
  }

  updateProject(project: Project) {
    return this.httpService.putFullRequest(this.apiPrefix, project);
  }

  deleteProject(id: number) {
    return this.httpService.deleteFullRequest(this.apiPrefix + "/" + id);
  }
}
