import { ExitUnsavedGuard } from "./../guards/exit-unsaved.guard";
import { ProjectDetailsComponent } from "./project-details/project-details.component";
import { ProjectComponent } from "./project.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProjectListComponent } from "./project-list/project-list.component";

const routes: Routes = [
  { path: "", component: ProjectComponent, children: [
      { path: "", component: ProjectListComponent, canDeactivate: [ExitUnsavedGuard]},
      { path: ":id", component: ProjectDetailsComponent, canDeactivate: [ExitUnsavedGuard]}
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule { }
