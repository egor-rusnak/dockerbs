import { ObservableUnsubscribe } from "./../../shared/observable-unsubscribe";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TeamService } from "./../../services/team.service";
import { UserService } from "./../../services/user.service";
import { takeUntil } from "rxjs/operators";
import { Project } from "./../../models/project";
import { ProjectService } from "./../../services/project.service";
import { forkJoin, Observable, Subject } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { ComponentCanDeactivate } from "src/app/guards/exit-unsaved.guard";
import { ActivatedRoute } from "@angular/router";
import { Team } from "src/app/models/team";
import { User } from "src/app/models/user";

@Component({
  selector: "app-project-details",
  templateUrl: "./project-details.component.html",
  styleUrls: ["./project-details.component.scss"],
})
export class ProjectDetailsComponent
  extends ObservableUnsubscribe
  implements OnInit, ComponentCanDeactivate {
  teams: Team[];
  users: User[];

  $loading = true;

  project: Project;
  projectForm: FormGroup;

  isEdit: boolean = false;
  constructor(
    private userService: UserService,
    private teamService: TeamService,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    const projectId = Number.parseInt(this.route.snapshot.paramMap.get("id"));
    forkJoin([
      this.teamService.getTeams(),
      this.userService.getUsers(),
      this.projectService.getProject(projectId),
    ])
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((response) => {
        this.teams = response[0];
        this.users = response[1];
        this.project = response[2];
        this.$loading = false;
      });
  }

  ngOnDesctroy() {
    this.completeSubscription();
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (this.isEdit) {
      return confirm(
        "You are editing a project, are you sure, you want to leave not saving it?"
      );
    } else {
      return true;
    }
  }

  editProject() {
    this.createForm();
    this.isEdit = true;
  }

  cancelEdit() {
    this.isEdit = false;
  }

  createForm() {
    this.projectForm = this.fb.group({
      id: [],
      name: [],
      description: [],
      deadline: [Validators.required],
      author: [Validators.required],
      team: [Validators.required],
      createdAt: [Validators.required],
    });
    this.projectForm.setValue(this.project);
  }

  onSubmit() {
    this.$loading = true;
    let project = this.projectForm.value as Project;
    this.projectService
      .updateProject(project)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(
        () => {
          this.project = project;
          this.isEdit = false;
          this.$loading = false;
        },
        (error) => console.log(error)
      );
  }

  compareTeams(t1: Team, t2: Team) {
    return t1.id === t2.id;
  }

  compareUsers(u1: User, u2: User) {
    return u1.id === u2.id;
  }
}
